# Color management and HDR documentation for FOSS graphics

Documentation in [this repository](https://gitlab.freedesktop.org/pq/color-and-hdr)
is intended to help with the design and implementation of color
management and HDR support on FOSS graphincs stacks, including
Mesa (EGL, Vulkan WSI), Linux (DRM KMS), Wayland (compositors and
applications), and even X11.

This is not an archive of proprietary documents like SMPTE, ITU, or VESA
specifications. All content must follow the [license](LICENSE).

## Contents

- [Wayland Color Management and HDR Design Goals](doc/design_goals.rst)
  describes the expectations and use cases that Wayland should meet.

- [Color Pipeline Overview](doc/winsys_color_pipeline.rst) compares the
  X11 and Wayland color pipelines, and explains how a Wayland
  compositor relates to display calibration.

- [Well-Known EOTFs, chromaticities and whitepoints](doc/well_known.rst)
  with links to specifications.

- [High Dynamic Range](doc/winsys_hdr.rst) in Wayland. Work in progress.

- [Glossary](doc/glossary.rst)

- [Todo](doc/todo.rst) contains random notes.

- [A Pixel's Color](doc/pixels_color.md) is an introduction to
  understanding pixel values and color.

## History

Originally this documentation was started to better explain how
[Wayland color management extension](https://gitlab.freedesktop.org/wayland/wayland-protocols/-/merge_requests/14)
should be used and what it means.

Widening community interestests particularly in HDR prompted for moving
the documentation into this separate repository to allow a more
streamlined way of contributing to it.

For now, this project lives in a personal space, but if it gets more
traction, moving it into an independent Gitlab group is possible without
losing any Issues or MRs.

## Releases

No releases are made from this repository. Use date or the git hash to
refer to specific revisions of the contents.

## Contributing

Open Issues and Merge Requests in Gitlab as usual. Use your own forked
repository for MR branches (@pq is exempt as long as this repository is
hosted in his personal Gitlab group). Each commit must carry
Signed-off-by tag to denote that the submitter adheres to
[Developer Certificate of Origin 1.1](https://developercertificate.org/).

All merge requests need to be accepted by at least one other person with
Developer or higher access level.

Reporter level access can be given by invite without any particular
requirements.

Developer access can be given on request, provided the person is
actively participating in discussions and has contributed an accepted
MR.

Maintainer access is given on Maintainers' collective discretion.

### Images

Please, also contribute the "sources" (e.g. SVG) of images if
reasonable, even when the image used by the document is an exported one,
e.g. PNG.

### Writing mathematics

If you want to typeset mathematics, use Gitlab flavored Markdown and its
[mathematics support](https://gitlab.freedesktop.org/help/user/markdown#math).
It uses [KaTeX](https://katex.org/) which supports
[a subset of LaTeX](https://katex.org/docs/supported.html).

[KaTeX demo](https://katex.org/#demo) may be helpful for polishing your
markup without needing to push to and view in Gitlab all the time.

## Conduct

This project follows
[the freedesktop.org Contributor Covenant](https://www.freedesktop.org/wiki/CodeOfConduct).

