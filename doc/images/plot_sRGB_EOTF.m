% SPDX-FileCopyrightText: 2021 Collabora, Ltd.
% SPDX-License-Identifier: MIT

% This is an Octave script: https://www.gnu.org/software/octave/

s = 0.04045;

e = [0 : 0.01 : 1];

o = zeros(size(e));

mask = e <= s;

o(mask) = e(mask) ./ 12.92;
o(~mask) = realpow((e(~mask) + 0.055) ./ 1.055, 2.4);

f = figure();
plot(e, o);
xticks([0:0.1:1])
yticks([0:0.1:1])
grid on
title('sRGB EOTF')
xlabel('electrical / non-linear value')
ylabel('optical / linear value')
axis square tight

ppi = get(0, 'ScreenPixelsPerInch');
set(f, 'PaperPosition', [0 0 400 400] ./ ppi)

print(f, 'sRGB_EOTF.png', '-dpng')
